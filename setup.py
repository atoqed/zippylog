import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='zippylog',
    url='https://gitlab.com/atoqed/zippylog',
    author='AuthorName',
    author_email='AuthorEmail',
    packages=['zippylog'],
    #install_requires=['numpy']
    version='0.1',
    license='MIT',
    description='A wrapper for pythons logger.',
    long_description=open('README.md').read(),
)
