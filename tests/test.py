import logging

from src import zippybuild


def main():
    zippybuild.run(config_filename='', config_dir='', log_dir='', console_level='INFO', env_key='LOG_CFG')
    logger = logging.getLogger(__name__)

    logger.info('I am an info message.')
    logger.debug('I am a debug message.')
    logger.warning('I am a warning message.')
    logger.error('I am an error message.')


if __name__ == '__main__':
    main()
