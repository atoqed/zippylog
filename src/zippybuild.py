import sys

import os
import json
import logging.config


def run(config_filename, config_dir, log_dir, console_level='INFO', env_key='LOG_CFG'):
    """
    setup logging configuration using a JSON file format
    :param config_filename: str, the name of the logging config file, with extension
    :param config_dir:
    :param log_dir:
    :param console_level: the level of logging that should be sent to the console
    :param env_key:
    :return: None, logging values are configured globally through this function
    """

    # init
    path = config_dir + '' + config_filename
    print(path)
    value = os.getenv(env_key, None)
    logging.captureWarnings(True)  # warnings.warn('...') will go to the log NOT Raise UserWarning('...')
    uncaught_logger = logging.getLogger('uncaught_exceptions')

    if console_level not in list(logging._levelToName.values()):
        console_level = 'INFO'

    if value:
        path = value

    if os.path.exists(path):
        # case when `logger_config.json` is in the proper location
        with open(path, 'rt') as f:
            config = json.load(f)

            config['handlers']['console']['level'] = console_level

            # update {info|debug|error}_file_handler log location if the directory exists
            try:
                config['handlers']['info_file_handler']['filename'] = log_dir + "/" + "info.log"
                config['handlers']['debug_file_handler']['filename'] = log_dir + "/" + "debug.log"
                config['handlers']['warning_file_handler']['filename'] = log_dir + "/" + "warning.log"
                config['handlers']['error_file_handler']['filename'] = log_dir + "/" + "error.log"
            except ValueError:
                logging.debug('[CONFIRM] ??? will be used to hold the logs until the log directory is '
                              'configured correctly. See README.md for more information.')

        try:
            logging.config.dictConfig(config)
        except ValueError:
            # when the log directory doesn't exist we need to create it first
            os.mkdir(log_dir)
            logging.config.dictConfig(config)
    else:
        # case when logger_config.json cannot be found
        logging.basicConfig(level=console_level)

    def exception_hook(exc_type, exc_value, exc_traceback):
        uncaught_logger.error('Uncaught exception', exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = exception_hook
